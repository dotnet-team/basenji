#!/bin/bash

# Import GNOME 3 thumbnailers into GConf.
# Workaround for bug lp:864615.
# https://bugs.launchpad.net/ubuntu/+source/gconf2/+bug/864615
function check_thumbnailers()
{
	THUMBNAILER_DIR=/usr/share/thumbnailers
	GCONF_PATH=/desktop/gnome/thumbnailers

	if [ $(gconftool-2 --all-dirs $GCONF_PATH | wc -l) -gt 0 ]; then
		# All good - thumbnailers are available in GConf
		return
	fi

	if [ ! -d $THUMBNAILER_DIR ]; then
		echo "Error: Can't import thumbnailers - directory '$THUMBNAILER_DIR' not found."
		return
	fi

	echo "Importing thumbnailers into GConf..."

	cnt=0
	commands=
	mimetypes=

	for f in $(ls $THUMBNAILER_DIR); do

		echo "* Importing $f"
	
		cmd=$(grep -w Exec "$THUMBNAILER_DIR/$f" | cut -d '=' -f2)
		mimestr=$(grep -w MimeType "$THUMBNAILER_DIR/$f" | cut -d '=' -f2)
	
		for m in $( echo $mimestr | tr ";" "\n"); do
			commands[$cnt]=$cmd
			mimetypes[$cnt]=$(echo $m | tr "/" "@" | tr "+" "@")
			((cnt++))
		done
	done

	echo "* Writing to GConf"
	entries=

	for (( i = 0 ; i < ${#mimetypes[@]} ; i++ )); do
	read -d '' entry <<EOF
		<entry>
		  <key>${mimetypes[$i]}/command</key>
		  <schema_key>/schemas$GCONF_PATH/${mimetypes[$i]}/command</schema_key>
		  <value>
		    <string>${commands[$i]}</string>
		  </value>
		</entry>
		<entry>
		  <key>${mimetypes[$i]}/enable</key>
		  <schema_key>/schemas$GCONF_PATH/${mimetypes[$i]}/enable</schema_key>
		  <value>
		    <bool>true</bool>
		  </value>
		</entry>
EOF
		entries=$entries$entry
	done

	read -d '' xml <<EOF
	<gconfentryfile>
	  <entrylist base="$GCONF_PATH">
		<entry>
		  <key>disable_all</key>
		  <schema_key>/schemas$GCONF_PATH/disable_all</schema_key>
		  <value>
		    <bool>false</bool>
		  </value>
		</entry>
		$entries
		</entrylist>
	</gconfentryfile>
EOF

	echo $xml | gconftool-2 --load -

	echo "Done."
}

check_thumbnailers

cd "@expanded_libdir@/@PACKAGE@"
exec mono Basenji.exe "$@"
